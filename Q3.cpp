#include<stdio.h>
#define MAX 50
int main()
{
	int a[MAX][MAX], b[MAX][MAX], c[MAX][MAX]={0}, d[MAX][MAX]={0};
	int i,j,k,m,n,p,q;
	printf("Add and multiply two given matrixes");
	printf("\n___________________________________");
	printf("\n\nEnter no. of rows and columns in matrix A: ");
	scanf("%d%d",&m,&n);
	printf("Enter elements of matrix A:\n");
		for(i=0;i<m;i++)
			for(j=0;j<n;j++)
				scanf("%d", &a[i][j]);
	
	printf("Enter no. of rows and columns in matrix B: ");
	scanf("%d%d",&p,&q);
		printf("Enter elements of matrix B:\n");
		for(i=0;i<p;i++)
			for(j=0;j<q;j++)
				scanf("%d", &b[i][j]);
	if(m!=p || n!=q)
	{
		printf("Matrix Addition is not possible");
		
	}
	else if(n!=p)
	{
		printf("Matrix Multiplication is not possible");
		
	}
	else
	{
		//Addition
		for(i=0;i<m;i++)
			for(j=0;j<n;j++)
				c[i][j] = a[i][j] + b[i][j];
		printf("\nMatirx Addition:\n");
		for(i=0;i<m;i++)
		{
			for(j=0;j<n;j++)
				printf("%d ", c[i][j]);
			printf("\n");
		}
		//Multiplication
		for(i=0;i<m;i++)
			for(j=0;j<q;j++)
				for(k=0;k<p;k++)
					d[i][j] += a[i][k]*b[k][j];
		printf("\nMatirx Multiplication:\n");
		for(i=0;i<m;i++)
		{
			for(j=0;j<q;j++)
				printf("%d ", d[i][j]);
			printf("\n");
		}
	}
}
