#include <stdio.h>
#include <string.h>
int main()
{
   char s[100];
   printf("Reverse a sentence entered by user");
   printf("\n___________________________________");
   printf("\n\nEnter a sentence :");
   gets(s);
   strrev(s);
   printf("Reverse of the sentence: %s", s);
   return 0;
}
